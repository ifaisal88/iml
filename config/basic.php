<?php

return [
    'maritial_status' => [
        'Single',
        'Married',
        'Divorced',
        'Seprated'
    ],
    'blood_group' => [
        'A+',
        'A-',
        'B+',
        'B-',
        'AB+',
        'AB-',
        'O+',
        'O-',
        'Rh(null)'
    ],
    'employee_gender' => [
        'Male',
        'Female'
    ],
    'probation_period' => [
        '1 Month',
        '2 Months',
        '3 Months',
        '4 Months',
        '5 Months',
        '6 Months'
    ]
];
