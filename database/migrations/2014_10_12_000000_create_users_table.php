<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->unique();
            $table->string('personal_id')->unique();
            $table->string('emirates_id')->unique();
            $table->unsignedBigInteger('employee_type_id')->nullable()->default(null);
            $table->unsignedBigInteger('visa_status_id')->nullable()->default(null);
            $table->unsignedBigInteger('employee_category_id')->nullable()->default(null);
            $table->unsignedBigInteger('employee_status_id')->nullable()->default(null);
            $table->unsignedBigInteger('company_id')->nullable()->default(null);
            $table->unsignedBigInteger('sponsor_id')->nullable()->default(null);
            $table->unsignedBigInteger('pay_group_id')->nullable()->default(null);
            $table->unsignedBigInteger('nationality_id')->nullable()->default(null);
            $table->unsignedBigInteger('religion_id')->nullable()->default(null);
            $table->string('line_manager_id')->nullable()->default(null);
            $table->unsignedBigInteger('department_id')->nullable()->default(null);
            $table->unsignedBigInteger('sub_department_id')->nullable()->default(null);
            $table->unsignedBigInteger('designation_id')->nullable()->default(null);
            $table->unsignedBigInteger('leave_per_annum_id')->nullable()->default(null);
            $table->unsignedBigInteger('role_id')->nullable()->default(null);
            $table->unsignedBigInteger('contract_type_id')->nullable()->default(null);
            $table->string('maritial_status')->nullable()->default(null);
            $table->string('probation_period')->nullable()->default(null);
            $table->date('date_of_birth')->nullable()->default(null);
            $table->string('blood_group')->nullable()->default(null);
            $table->string('maiden_name')->nullable()->default(null);
            $table->string('gender')->nullable()->default(null);
            $table->string('first_name');
            $table->string('middle_name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            $table->string('full_name');
            $table->string('parent_name')->nullable()->default(null);;
            $table->string('email')->unique();
            $table->string('profile_image')->nullable()->default(null);
            $table->string('password');
            $table->string('personal_email')->nullable()->default(null);
            $table->string('mobile_number')->nullable()->default(null);
            $table->string('home_phone_number');
            $table->string('work_mobile')->nullable()->default(null);
            $table->string('current_address')->nullable()->default(null);
            $table->string('home_country_address')->nullable()->default(null);
            $table->date('date_of_joining')->nullable()->default(null);
            $table->date('date_of_exit')->nullable()->default(null);
            $table->boolean('status')->default(true);
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
