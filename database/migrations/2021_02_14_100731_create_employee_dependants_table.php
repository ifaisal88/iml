<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeDependantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_dependants', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->unsignedBigInteger('relation_id');
            $table->date('date_of_birth')->nullable()->default(null);;
            $table->string('name');
            $table->unsignedBigInteger('country_id')->nullable()->default(null);;
            $table->string('passport_number')->nullable()->default(null);;
            $table->date('passport_expiry')->nullable()->default(null);;
            $table->string('visa_number')->nullable()->default(null);;
            $table->date('visa_expiry')->nullable()->default(null);;
            $table->boolean('travel_allounce')->nullable()->default(null);;
            $table->boolean('insurance')->nullable()->default(null);;
            $table->string('remarks')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('user_employee_id')
                ->references('employee_id')
                ->on('users');

            $table->foreign('relation_id')
                ->references('id')
                ->on('relations');

            $table->foreign('country_id')
                ->references('id')
                ->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_dependants');
    }
}
