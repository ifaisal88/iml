<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('employee_type_id')
                ->references('id')
                ->on('employee_types');

            $table->foreign('visa_status_id')
                ->references('id')
                ->on('visa_statuses');

            $table->foreign('employee_category_id')
                ->references('id')
                ->on('employee_categories');

            $table->foreign('employee_status_id')
                ->references('id')
                ->on('employee_statuses');

            $table->foreign('company_id')
                ->references('id')
                ->on('companies');

            $table->foreign('sponsor_id')
                ->references('id')
                ->on('sponsors');

            $table->foreign('pay_group_id')
                ->references('id')
                ->on('pay_groups');

            $table->foreign('nationality_id')
                ->references('id')
                ->on('countries');

            $table->foreign('religion_id')
                ->references('id')
                ->on('religions');

            $table->foreign('line_manager_id')
                ->references('employee_id')
                ->on('users');

            $table->foreign('department_id')
                ->references('id')
                ->on('departments');

            $table->foreign('sub_department_id')
                ->references('id')
                ->on('sub_departments');

            $table->foreign('designation_id')
                ->references('id')
                ->on('designations');

            $table->foreign('created_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('role_id')
                ->references('id')
                ->on('roles');

            $table->foreign('contract_type_id')
                  ->refrences('id')
                  ->on('contract_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            // Schema::dropIfExists('users');
            $table->dropForeign('users_employee_type_id_foreign');
            $table->dropForeign('users_visa_status_id_foreign');
            $table->dropForeign('users_employee_category_id_foreign');
            $table->dropForeign('users_employee_status_id_foreign');
            $table->dropForeign('users_company_id_foreign');
            $table->dropForeign('users_sponsor_id_foreign');
            $table->dropForeign('users_pay_group_id_foreign');
            $table->dropForeign('users_nationality_id_foreign');
            $table->dropForeign('users_religion_id_foreign');
            $table->dropForeign('users_line_manager_id_foreign');
            $table->dropForeign('users_department_id_foreign');
            $table->dropForeign('users_sub_department_id_foreign');
            $table->dropForeign('users_designation_id_foreign');
            $table->dropForeign('users_created_by_foreign');
            $table->dropForeign('users_updated_by_foreign');
            $table->dropForeign('users_role_id_foreign');
        });
    }
}