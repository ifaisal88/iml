<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EmployeeDependantController;
use App\Http\Controllers\ContactPersonController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__.'/auth.php';

Route::middleware(['auth'])->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/test', [UserController::class, 'test'])->name('test');
    
    Route::resource('/dependants', EmployeeDependantController::class);
    Route::resource('/contactPerson', ContactPersonController::class);
    
    
    Route::resource('/employees', EmployeeController::class);
    Route::group(['prefix' => 'employees','as' => 'employees.'],function(){
        Route::post('/{id}/contact-person', [EmployeeController::class, 'storeContactPerson'])->name('store.contactPerson');
        Route::post('/{id}/dependants', [EmployeeController::class, 'storeDependants'])->name('store.dependants');
        Route::post('/{id}/education', [EmployeeController::class, 'storeEducation'])->name('store.educationDetail');
        Route::post('/{id}/bank-details', [EmployeeController::class, 'storeAccountDetails'])->name('store.accountDetails');
        Route::post('/{id}/documents', [EmployeeController::class, 'storeDocuments'])->name('store.documents');
        Route::post('/{id}/assets', [EmployeeController::class, 'storeAssets'])->name('store.assets');
        Route::post('/{id}/dependant-benefits', [EmployeeController::class, 'storeDependantBenefits'])->name('store.dependantBenefits');
        Route::put('/{id}/main-info', [EmployeeController::class, 'updateEmployeeMainInfo'])->name('update.mainInfo');
        Route::put('/{id}/personal-details', [EmployeeController::class, 'updateEmployeePersonalDetails'])->name('update.personalDetails');
        Route::put('/{id}/employee-contact', [EmployeeController::class, 'updateEmployeeContactDetails'])->name('update.contactDetails');
        Route::put('/{id}/employee-official-details', [EmployeeController::class, 'updateEmployeeOfficialDetails'])->name('update.officialDetails');
        Route::put('/{id}/employee-profile-image', [EmployeeController::class, 'updateProfileImage'])->name('update.profileImage');
        Route::put('/{id}/employee-rules', [EmployeeController::class, 'updateEmployeeRules'])->name('update.employeeRules');
    });
});