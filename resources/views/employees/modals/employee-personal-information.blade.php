<form method="POST" action="{{ route('employees.update.personalDetails', [$employee->employee_id]) }}">
    @csrf
    @method('PUT')
    <div class="modal fade" id="employee-personal-information" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Employee Personal Information</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nationality_id">Nationality</label>
                                <select class="simple-select2 w-100" id="nationality_id" name="nationality_id">
                                    <option value="">Nationality</option>
                                    @foreach($nationalities as $nationality)
                                        <option value="{{$nationality->id}}" {{($employee->nationality_id == $nationality->id)? 'selected' : ''}}>{{$nationality->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="relegion_id">Religion</label>
                                <select class="simple-select2 w-100" id="religion_id" name="religion_id">
                                    <option value="">Religion</option>
                                    @foreach($religions as $religion)
                                        <option value="{{$religion->id}}" {{($employee->religion_id == $religion->id)?'selected':''}}>{{$religion->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="maritial_status">Marital Status</label>
                                <select class="simple-select2 w-100" id="maritial_status" name="maritial_status">
                                    <option value=""></option>
                                    @foreach($maritialStatus as $status)
                                    <option value="{{$status}}" {{($employee->maritial_status == $status)?'selected':''}}>{{$status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="blood_group">Blood Group</label>
                                <select class="simple-select2 w-100" id="blood_group" name="blood_group">
                                    <option value=""></option>
                                    @foreach($bloodGroup as $group)
                                    <option value="{{ $group }}" {{($employee->blood_group == $group)?'selected':''}}>{{ $group }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth">Date of Birth</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-calendar"></i></span>
                                    </div>
                                    <input name="date_of_birth" id="date_of_birth" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" class="form-control" placeholder="Date of Birth" value="{{$employee->date_of_birth}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <select class="simple-select2 w-100" id="gender" name="gender">
                                    <option value=""></option>
                                    @foreach($employeeGender as $gender)
                                    <option value="{{ $gender }}" {{($employee->gender == $gender)?'selected':''}}>{{ $gender }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="maiden_name">Maiden Name</label>
                                <input type="text" class="form-control" name="maiden_name" id="maiden_name" placeholder="Maiden Name" value="{{($employee->maiden_name == '')? '' : $employee->maiden_name}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="parent_name">Father/Mother Name</label>
                                <input type="text" class="form-control" name="parent_name" id="parent_name" placeholder="Father/Mother Name" value="{{($employee->parent_name == '')? '' : $employee->parent_name}}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" id="modal-footer">
                        <div id="dependant_div">
                            <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>
		</div>
    </div>
</form>