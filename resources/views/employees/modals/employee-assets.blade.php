<form action="{{ route('employees.store.assets', [$employee->employee_id]) }}" method="post">
    @csrf
    <div class="modal fade" id="employee-assets" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="smallModalLabel">Employee Assets</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-4 pull-left">
                        <label for="asset_type_id">Asset Type</label>
                        <select name="asset_type_id" id="asset_type_id" class="simple-select2 w-100">
                            <option value="">Asset Type</option>
                            @foreach($asset_types as $asset_type)
                            <option value="{{ $asset_type->id }}">{{ $asset_type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="asset_brand_id">Brand</label>
                        <select name="asset_brand_id" id="asset_brand_id" class="simple-select2 w-100">
                            <option value="">Brand</option>
                            @foreach($asset_brands as $asset_brand)
                            <option value="{{ $asset_brand->id }}">{{ $asset_brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="asset_condition_id">Condition</label>
                        <select name="asset_condition_id" id="asset_condition_id" class="simple-select2 w-100">
                            <option value="">Condition</option>
                            @foreach($asset_conditions as $asset_condition)
                            <option value="{{ $asset_condition->id }}">{{ $asset_condition->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="serial_no">Serial No.</label>
                        <input type="text" name="serial_no" id="serial_no" class="form-control" placeholder="Serial No.">
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="currency_id">Currency</label>
                        <select name="currency_id" id="currency_id" class="simple-select2 w-100">
                            <option value="">Currency</option>
                            @foreach($currencies as $currency)
                            <option value="{{ $currency->id }}">{{ $currency->symbol }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-8 pull-left">
                        <label for="value">Value</label>
                        <input type="text" name="value" id="value" class="form-control" placeholder="Value">
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description"></textarea>
                    </div>
                    <div class="form-group col-6 pull-left"">
                        <label for="issue_date">Issue Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="issue_date" id="issue_date" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Issue Date">
                        </div>
                    </div>
                    <div class="form-group col-6 pull-left"">
                        <label for="return_date">Return Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="return_date" id="return_date" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Return Date">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>