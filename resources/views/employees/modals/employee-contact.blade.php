<form action="{{ route('employees.update.contactDetails', [$employee->employee_id]) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="modal fade" id="employee-contact" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Employee Contact Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-md-6 pull-left">
                        <label for="email">Company Email</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Company Email" value="{{($employee->email == '')? '' : $employee->email }}">
                    </div>
                    <div class="form-group col-md-6 pull-left">
                        <label for="personal_email">Personal Email</label>
                        <input type="text" class="form-control" name="personal_email" id="personal_email" placeholder="Personal Email" value="{{($employee->personal_email == '')? '' : $employee->personal_email }}">
                    </div>
                    <div class="form-group col-md-4 pull-left">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="text" class="form-control" name="mobile_number" id="mobile_number" placeholder="Mobile Number" value="{{($employee->mobile_number == '')? '' : $employee->mobile_number }}">
                    </div>
                    <div class="form-group col-md-4 pull-left">
                        <label for="home_phone_number">Home Phone</label>
                        <input type="text" class="form-control" name="home_phone_number" id="home_phone_number" placeholder="Home Phone" value="{{($employee->home_phone_number == '')? '' : $employee->home_phone_number }}">
                    </div>
                    <div class="form-group col-md-4 pull-left">
                        <label for="work_mobile">Work Number</label>
                        <input type="text" class="form-control" name="work_mobile" id="work_mobile" placeholder="Work Number" value="{{($employee->work_mobile == '')? '' : $employee->work_mobile }}">
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="current_address">Current Address</label>
                        <textarea name="current_address" id="current_address" class="form-control">{{($employee->current_address == '')? '' : $employee->current_address }}</textarea>
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="home_country_address">Home Country Address</label>
                        <textarea name="home_country_address" id="home_country_address" class="form-control">{{($employee->home_country_address == '')? '' : $employee->home_country_address }}</textarea>
                    </div>
			    </div>                
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
		    </div>
        </div>
    </div>
</form>