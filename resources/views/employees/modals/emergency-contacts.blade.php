<form action="{{ route('employees.store.contactPerson', [$employee->employee_id]) }}" method="POST">
    @csrf
    <div class="modal fade" id="emergency-contacts" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Emergency Contact Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-6 pull-left">
                        <label for="full_name">Full Name</label>
                        <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name" value="">
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                    </div>
                    <div class="form-group col-12 pull-left">
						<label for="relation">Relation</label>
						<select name="relation" class="simple-select2 w-100">
							<option value="">Relation</option>
                            @foreach($relations as $relation)
                                <option value="{{ $relation->id }}">{{ $relation->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="text" class="form-control" name="mobile_number" id="mobile_number" placeholder="Mobile Number">
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="home_phone_number">Home Number</label>
                        <input type="text" class="form-control" name="home_phone_number" id="home_phone_number" placeholder="Home Number">
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="work_number">Work Number</label>
                        <input type="text" class="form-control" name="work_number" id="work_number" placeholder="Work Number">
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="address">Address</label>
                        <textarea name="address" id="address" class="form-control"></textarea>
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
			    </div>
                <div class="modal-footer" id="modal-footer">
                    <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
		    </div>
        </div>
    </div>
</form>