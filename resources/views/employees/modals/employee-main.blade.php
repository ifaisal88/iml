<form method="POST" action="{{ route('employees.update.mainInfo', [$employee->employee_id]) }}">
    @csrf
    @method('PUT')
    <div class="modal fade" id="employee-main" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Employee Main Summary</h4>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="line-manager">Line Manager</label>
                                <select class="simple-select2 w-100" id="line-manager" name="line_manager_id">
                                    <option value="">Line Manager</option>
                                    @foreach($line_managers as $line_manager)
                                        <option value="{{$line_manager->employee_id}}" {{($employee->line_manager_id == $line_manager->employee_id)?'selected':''}}>{{$line_manager->full_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="company_id">Company</label>                                 
                                <select class="simple-select2 w-100" id="company_id" name="company_id">
                                    <option value="">Company</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}" {{($employee->company_id == $company->id)?'selected':''}}>{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group"> 
                                <label for="sponser_id">Sponsor</label>                                 
                                <select class="simple-select2 w-100" id="sponser_id" name="sponsor_id">
                                    <option value="">Sponsor</option>
                                    @foreach($sponsors as $sponsor)
                                        <option value="{{$sponsor->id}}" {{($employee->sponsor_id == $sponsor->id)?'selected':''}}>{{$sponsor->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="pay_group_id">Pay Group</label>                                  
                                <select class="simple-select2 w-100" id="pay_group_id" name="pay_group_id">
                                    <option value="">Pay Group</option>
                                    @foreach($pay_groups as $pay_group)
                                        <option value="{{$pay_group->id}}" {{($employee->pay_group_id == $pay_group->id)?'selected':''}}>{{$pay_group->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
					
                    <div class="modal-footer" id="modal-footer">
                        <div id="dependant_div">
                            <button type="submit" class="btn btn-primary">SAVE CHANGES</button>                
                            <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
				</div>
			</div>
		</div>
    </div>
</form>