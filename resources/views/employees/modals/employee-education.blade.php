<form action="{{ route('employees.store.educationDetail', [$employee->employee_id]) }}" method="POST" enctype='multipart/form-data'>
    @csrf
    <div class="modal fade" id="employee-education" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="smallModalLabel">Employee Education Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-12 pull-left">
                        <label for="qualification_id">Qualification</label>
                        <select name="qualification_id" id="qualification_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($qualifications as $qualification)
                                <option value="{{ $qualification->id }}">{{ $qualification->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="institute_id">Institute</label>
                        <select name="institute_id" id="institute_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($institutes as $institute)
                                <option value="{{ $institute->id }}">{{ $institute->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="country_id">Country</label>
                        <select name="country_id" id="country_id" class="simple-select2 w-100">
                            <option value=""></option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="marks">Marks/Grade</label>
                        <input type="text" name="marks" id="marks" class="form-control" placeholder="Marks/Grade">
                    </div>
                    <div class="form-group col-md-6 pull-left">
                        <label for="completed_date">Date of Completion</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="completed_date" id="completed_date" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Date of Completion">
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <div class="fancy-checkbox pull-left">
                            <label>
                                <input name="attested" value="1" type="checkbox">
                                <span>Attested</span>
                            </label>
                        </div>
                        <div class="fancy-checkbox pull-left">
                            <label>
                                <input name="hardcopy" value="1" type="checkbox">
                                <span>Hardcopy</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="degree_image">Degree Image to Upload</label>
                        <input type="file" name="degree" id="degree" class="dropify">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>