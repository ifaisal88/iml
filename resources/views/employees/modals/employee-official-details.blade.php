<form action="{{ route('employees.update.officialDetails', [$employee->employee_id]) }}" method="POST">
    @csrf
    @method('PUT')
	<div class="modal fade" id="official-details" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="title" id="defaultModalLabel">Employee Official Details</h4>
				</div>
				<div class="modal-body">
					<div class="form-group col-6 pull-left">
						<label for="employee_category_id">Position Category</label>
						<select name="employee_category_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($positionCategories as $category)
                                <option value="{{ $category->id }}" {{($employee->employee_category_id == $category->id)?'selected':''}}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
					<div class="form-group col-6 pull-left">
						<label for="designation_id">Designation</label>
						<select name="designation_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($designations as $designation)
                                <option value="{{ $designation->id }}" {{($employee->designation_id == $designation->id)?'selected':''}}>{{ $designation->name }}</option>
                            @endforeach
                        </select>
                    </div>
					<div class="form-group col-6 pull-left">
						<label for="department_id">Department</label>
						<select name="department_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($departments as $department)
                                <option value="{{ $department->id }}" {{($employee->department_id == $department->id)?'selected':''}}>{{ $department->name }}</option>
                            @endforeach
                        </select>
                    </div>
					<div class="form-group col-6 pull-left">
						<label for="sub_department_id">Sub Department</label>
						<select name="sub_department_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($subDepartments as $subDepartment)
                                <option value="{{ $subDepartment->id }}" {{($employee->sub_department_id == $subDepartment->id)?'selected':''}}>{{ $subDepartment->name }}</option>
                            @endforeach
                        </select>
                    </div>
					<div class="form-group col-6 pull-left">
						<label for="country_id">Country</label>
						<select name="country_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{($employee->country_id == $country->id)?'selected':''}}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
					<div class="form-group col-6 pull-left">
						<label for="city_id">City</label>
						<select name="city_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}" {{($employee->city_id == $city->id)?'selected':''}}>{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="date_of_joining">Date of Joining</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="date_of_joining" value="{{ $employee->date_of_joining == ''? '' : $employee->date_of_joining}}" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Date">
                        </div>
                    </div>
                    <div class="form-group col-4 pull-left">
						<label for="probation_period">Probation Period</label>
						<select name="probation_period" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($probationPeriod as $period)
                                <option value="{{ $period }}" {{($employee->probation_period == $period)?'selected':''}}>{{ $period }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-4 pull-left">
                        <label for="probation_end_data">Probation End On</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="probation_end_data" value="{{ $employee->probation_end_data == ''? '' : $employee->probation_end_data}}" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Date">
                        </div>
                    </div>
                    <div class="form-group col-6 pull-left">
						<label for="cost_center_id">Cost Center</label>
						<select name="cost_center_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($costCenters as $costCenter)
                                <option value="{{ $costCenter->id }}" {{($employee->cost_center_id == $costCenter->id)?'selected':''}}>{{ $costCenter->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 pull-left">
						<label for="project_id">Project/Client</label>
						<select name="project_id" class="simple-select2 w-100">
							<option value=""></option>
                            @foreach($projects as $project)
                                <option value="{{ $project->id }}" {{($employee->project_id == $project->id)?'selected':''}}>{{ $project->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 pull-left">
						<label for="supervisor">Is a Supervisor?</label>
						<select name="supervisor" class="simple-select2 w-100">
							<option value="" selected>Is Supervisor?</option>
                            <option value="1" {{($employee->supervisor == 1)?'selected':''}}>Yes</option>
                            <option value="0" {{($employee->supervisor == 0)?'selected':''}}>No</option>
                        </select>
                    </div>
                    <div class="form-group col-md-5 pull-left">
                        <label for="date_of_exit">Date of Exit <small>(If Applicable)</small></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input disabled name="date_of_exit" value="{{ $employee->date_of_exit == ''? '' : $employee->date_of_exit}}" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Date of Exit">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>