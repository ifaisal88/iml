@extends('layouts.templates')
@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i lass="fa fa-arrow-left"></i></a> Employee List</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">Employee</li>
                            <li class="breadcrumb-item active">Employee List</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Employee List</h2>
                            <ul class="header-dropdown">
                                <li><button class="btn btn-primary ml-auto" data-toggle="modal" data-target="#add-employee">Add Employee</button></li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom table-striped m-b-0 c_list">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th></th>
                                            <th>Name</th>
                                            <th>Employee ID</th>
                                            <th>Phone</th>
                                            <th>Join Date</th>
                                            <th>Department</th>
                                            <th>Designation</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($employees as $employee)
                                            <tr>
                                                <td class="width45">
                                                    @php
                                                        $path = Storage::disk('media_images')->exists($employee->employee_id . '/profile.jpg') ? asset('media_images/' . $employee->employee_id . '/profile.jpg') : asset('media_images/profile.jpg');
                                                    @endphp
                                                    <img src="{{$path}}" class="rounded-circle avatar" alt=""/>
                                                </td>
                                                <td>
                                                    <h6 class="mb-0">{{ $employee->full_name }}</h6>
                                                    <span>{{ $employee->email }}</span>
                                                </td>
                                                <td><span>{{ $employee->employee_id }}</span></td>
                                                <td><span>{{ $employee->home_phone_number ?? 'N/A' }}</span></td>
                                                <td>{{ $employee->date_of_joining ?? 'N/A' }}</td>
                                                <td>{{ $employee->department->name ?? 'N/A' }}</td>
                                                <td>{{ $employee->designation->name ?? 'N/A' }}</td>
                                                <td>
                                                    <form action="{{ route('employees.destroy', [$employee]) }}"
                                                        method="POST">
                                                        @method('DELETE')
                                                        <a href="{{ route('employees.show', [$employee]) }}"><button
                                                                type="button" class="btn btn-sm btn-outline-secondary"
                                                                title="Edit"><i class="fa fa-eye"></i></button></a>
                                                        <button type="button"
                                                            class="btn btn-sm btn-outline-danger js-sweetalert"
                                                            title="Delete" data-type="confirm"
                                                            data-value="{{ $employee->employee_id }}"
                                                            data-model="employees"
                                                            value="{{ $employee->employee_id }}"><i
                                                                class="fa fa-trash-o"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Default Size -->
    @include('employees.modals.add-employee')

@endsection
