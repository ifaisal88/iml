@extends('layouts.templates')
@section('extra-css')

    <link rel="stylesheet" href="{{ asset('assets/vendor/dropify/css/dropify.min.css') }}">
    <style>
        .hideDiv {
            display: none;
        }

    </style>
@endsection
@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a></h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('employees.index') }}">Employees</a></li>
                            <li class="breadcrumb-item active">Employee Profile</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card w_profile">
                        <div class="body">
                            <div class="row">
                                <div class="col">
                                    <div class="profile-image">
                                        @php
                                            $path = Storage::disk('media_images')->exists($employee->employee_id.'/profile.jpg')?asset('media_images/'.$employee->employee_id.'/profile.jpg'):asset('media_images/profile.jpg');
                                        @endphp
                                        <a href="" data-toggle="modal" data-target="#profile-image">
                                            <img src="{{$path}}" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-10">
                                    <div class="row">
                                        <div class="col-6" style="margin-bottom: 35px">
                                            <h4 class="m-t-0 m-b-0"><strong>{{ $employee->full_name }}</strong></h4>
                                            <span class="job_post"><strong>Emp ID:</strong> {{ $employee->employee_id }} </span>
                                            {{-- <p class="social-icon">
                                                <a title="Facebook" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
                                                <a title="Google-plus" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>
                                                <a title="Instagram" href="javascript:void(0);"><i class="fa fa-instagram "></i></a>
                                            </p> --}}
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-primary ml-auto pull-right" id="showForm"><i class="fa fa-pencil-square-o"></i> Edit</button>
                                            <button type="button" class="btn btn-success ml-auto pull-right hideDiv" id="updateForm"><i class="fa fa-arrow-up"></i> Update</button>
                                        </div>
                                    </div>
                                    <div class="row profileDetails">
                                        <div class="col-2">
                                            <div class="text-black-50">Personal ID</div>
                                            <h6>{{ $employee->personal_id ?? 'N/A' }}</h6>
                                        </div>
                                        <div class="col-2">
                                            <div class="text-black-50">Emirates ID</div>
                                            <h6>{{ $employee->emirates_id ?? 'N/A' }}</h6>
                                        </div>
                                        <div class="col-2">
                                            <div class="text-black-50">Employee Type</div>
                                            <h6>{{ $employee->employeeType->name ?? 'N/A' }}</h6>
                                        </div>
                                        <div class="col-2">
                                            <div class="text-black-50">Visa Status</div>
                                            <h6>{{ $employee->visaStatus->name ?? 'N/A' }}</h6>
                                        </div>
                                        <div class="col-2">
                                            <div class="text-black-50">Category</div>
                                            <h6>{{ $employee->employeeCategory->name ?? 'N/A' }}</h6>
                                        </div>
                                        <div class="col-2">
                                            <div class="text-black-50">Employee Status</div>
                                            <h6>{{ $employee->employeeStatus->name ?? 'N/A' }}</h6>
                                        </div>
                                    </div>
                                    <div class="row editFormArea hideDiv">
                                        <form id="updateProfile" class="col-12">
                                            <div class="row">
                                                <div class="col-2 form-group">
                                                    <label for="first_name" class=" text-black-50">First Name</label>
                                                    <input class="form-control" value="{{ $employee->first_name }}"
                                                        id="first_name" name="first_name" />
                                                </div>
                                                <div class="col-2 form-group">
                                                    <label for="middle_name" class=" text-black-50">Middle Name</label>
                                                    <input class="form-control" value="{{ $employee->middle_name }}"
                                                        id="middle_name" name="first_name" />
                                                </div>
                                                <div class="col-2 form-group">
                                                    <label for="last_name" class=" text-black-50">Last Name</label>
                                                    <input class="form-control" value="{{ $employee->last_name }}"
                                                        id="last_name" name="first_name" />
                                                </div>
                                                <div class="col-2 form-group">
                                                    <label for="personal_id" class=" text-black-50">Personal ID</label>
                                                    <input class="form-control" value="{{ $employee->personal_id }}"
                                                        id="personal_id" name="personal_id" />
                                                </div>
                                                <div class="col-2 form-group">
                                                    <label for="emirates_id" class=" text-black-50">Emirates ID</label>
                                                    <input class="form-control" value="{{ $employee->emirates_id }}"
                                                        id="emirates_id" name="emirates_id" />
                                                </div>
                                                <div class="col-2 form-group">
                                                    <label for="employee_type" class="text-black-50">Employee Type</label>
                                                    <select class="simple-select2 w-100" id="employee_type_id" name="employee_type_id">
                                                        <option value=""></option>
                                                        @foreach ($employee_types as $type)
                                                            <option value="{{ $type->id }}" {{ ($employee->employee_type_id == $type->id)? 'selected' : '' }}>{{ $type->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-2 form-group">
                                                    <label for="visa_status" class=" text-black-50">Visa Status</label>
                                                    <select class="simple-select2 w-100" id="visa_status" name="visa_status">
                                                        <option value=""></option>
                                                        @foreach ($visa_statuses as $status)
                                                            <option value="{{ $status->id }}" {{ ($employee->visa_status_id == $status->id)? 'selected' : '' }}>{{ $status->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-2 form-group">
                                                    <label for="employee_category_id" class=" text-black-50">Category</label>
                                                    <select class="simple-select2 w-100" id="employee_category_id" name="employee_category_id">
                                                        <option value=""></option>
                                                        @foreach ($employee_categories as $category)
                                                            <option value="{{ $category->id }}" {{ ($employee->employee_category_id == $category->id)? 'selected' : '' }}>{{ $category->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-2 form-group">
                                                    <label for="employee_status_id" class=" text-black-50">Employee Status</label>
                                                    <select class="simple-select2 w-100" id="employee_status_id" name="employee_status_id">
                                                        <option value=""></option>
                                                        @foreach ($employeeStatuses as $status)
                                                            <option value="{{ $status->id }}" {{ ($employee->employee_status_id == $status->id)? 'selected' : '' }}>{{ $status->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="card single_post">
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#empmain"> Main</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#personalinfo"> Personal Info</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#empcontact"> Employee Contact</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#empdependents"> Dependents</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#contacts"> Emergency Contacts</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#empeducation"> Education</a></li>
                </ul>
                <div class="tab-content bg-dark text-light">
                    <!-- Employee Main Start -->
                    <div class="tab-pane show active" id="empmain">
                        <h6 class='text-warning'><b>Main </b><small> (Employee Summary)</small>
                        <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-main"><i class="fa fa-pencil-square-o"></i> Edit</button>
                        </h6>
                        <hr />
                        <div class="row" style="margin-bottom: 20px">
                            <div class="col-3 pull-left">
                                <h6>Line Manager</h6>
                                <p>{{ $employee->linemanager->full_name ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Company</h6>
                                <p>{{ $employee->company->name ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Sponsor</h6>
                                <p>{{ $employee->sponsor->name ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Pay Group</h6>
                                <p>{{ $employee->paygroup->name ?? 'N/A' }}</p>
                            </div>
                        </div>
                        
                        {{-- <div class="table-responsive">
                            <table class="table m-b-0">
                                <thead>
                                    <tr style="font-weight:normal;">
                                        <th>Line Manager</th>
                                        <th>Company</th>
                                        <th>Sponsor</th>
                                        <th>Pay Group</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $employee->linemanager->full_name ?? 'N/A' }}</td>
                                        <td>{{ $employee->company->name ?? 'N/A' }}</td>
                                        <td>{{ $employee->sponsor->name ?? 'N/A' }}</td>
                                        <td>{{ $employee->paygroup->name ?? 'N/A' }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> --}}
                    </div>
                    <!-- Employee Main End -->
                    <!-- Employee Personal Info Start -->
                    <div class="tab-pane show" id="personalinfo">
                        <h6 class='text-warning'><b>Employee Information </b><small> (Personal Information of Employee)</small>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-personal-information"><i class="fa fa-pencil-square-o"></i> Edit</button>
                        </h6>
                        <hr />
                        <div class="row" style="margin-bottom: 20px">
                            <div class="col-3 pull-left">
                                <h6>Nationality</h6>
                                <p>{{ $employee->nationality->name ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Religion</h6>
                                <p>{{ $employee->religion->name ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Marital Status</h6>
                                <p>{{ $employee->maritial_status ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Blood Group</h6>
                                <p>{{ $employee->blood_group ?? 'N/A' }}</p>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-3 pull-left">
                                <h6>Birth Date</h6>
                                <p>{{ $employee->date_of_birth ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Maiden Name</h6>
                                <p>{{ $employee->maiden_name ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Father/Mother Name</h6>
                                <p>{{ $employee->parent_name ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Gender</h6>
                                <p>{{ $employee->gender ?? 'N/A' }}</p>
                            </div>
                        </div>
                        {{-- <div class="table-responsive">
                            <table class="table m-b-0">
                                <thead>
                                    <tr style="font-weight:normal;">
                                        <th>Nationality</th>
                                        <th>Religion</th>
                                        <th>Marital Status</th>
                                        <th>Blood Group</th>
                                        <th>Birth Date</th>
                                        <th>Maiden Name</th>
                                        <th>Father/Mother Name</th>
                                        <th>Gender</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $employee->nationality->name??'' }}</td>
                                        <td>{{ $employee->religion->name??'' }}</td>
                                        <td>{{ $employee->maritial_status }}</td>
                                        <td>{{ $employee->blood_group }}</td>
                                        <td>{{ $employee->date_of_birth }}</td>
                                        <td>{{ $employee->maiden_name }}</td>
                                        <td>{{ $employee->parent_name }}</td>
                                        <td>{{ $employee->gender }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> --}}
                    </div>
                    <!-- Employee Personal Info End -->
                    <!-- Employee Contact Details Start -->
                    <div class="tab-pane show" id="empcontact">
                        <h6 class='text-warning'><b>Employee Contact</b><small> (Personal Contact Details)</small>
                        <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-contact"><i class="fa fa-pencil-square-o"></i> Edit</button>
                        </h6>
                        <hr />

                        <div class="row" style="margin-bottom: 20px">
                            <div class="col-3 pull-left">
                                <h6>Company Email</h6>
                                <p>{{ $employee->email }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Personal Email</h6>
                                <p>{{ $employee->personal_email ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Mobile Number</h6>
                                <p>{{ $employee->mobile_number ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Home Phone</h6>
                                <p>{{ $employee->home_phone_number ?? 'N/A' }}</p>
                            </div>
                        </div>
                        <hr />
                        <div class="row" style="margin-bottom: 20px">
                            <div class="col-3 pull-left">
                                <h6>Work Mobile <small>(if applicable)</small></h6>
                                <p>{{ $employee->work_mobile ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Current Address</h6>
                                <p>{{ $employee->current_address ?? 'N/A' }}</p>
                            </div>
                            <div class="col-3 pull-left">
                                <h6>Home Country Address</h6>
                                <p>{{ $employee->home_country_address ?? 'N/A' }}</p>
                            </div>
                        </div>
                        
                        {{-- <div class="table-responsive">
                            <table class="table m-b-0">
                                <thead>
                                    <tr style="font-weight:normal;">
                                        <th>Company Email</th>
                                        <th>Personal Email</th>
                                        <th>Mobile Number</th>
                                        <th>Home Phone</th>
                                        <th>Work Mobile <small>(if applicable)</small></th>
                                        <th>Current Address</th>
                                        <th>Home Country Address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $employee->email }}</td>
                                        <td>{{ $employee->personal_email }}</td>
                                        <td>{{ $employee->mobile_number }}</td>
                                        <td>{{ $employee->home_phone_number }}</td>
                                        <td>{{ $employee->work_mobile }}</td>
                                        <td>{{ $employee->current_address }}</td>
                                        <td>{{ $employee->home_country_address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> --}}
                    </div>
                    <!-- Employee Contact Details End-->
                    <!-- Employee Dependants Start -->
                    <div class="tab-pane show" id="empdependents">
                        <h6 class='text-warning'><b>Employee Dependents </b><small> (Upload Documents where applicable)</small>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-dependants"><i class="fa fa-plus"></i> Add</button>
                        </h6>
                        <hr />
                        <div class="table-responsive">
                            <table class="table m-b-0">
                                <thead>
                                    <tr style="font-weight:normal;">
                                        <th>Relation</th>
                                        <th>Dependent Name</th>
                                        <th>Date Of Birth</th>
                                        <th>Country</th>
                                        <th>Passport No.</th>
                                        <th>Expiry</th>
                                        <th>Visa No.</th>
                                        <th>Expiry</th>
                                        <th>Travel</th>
                                        <th>Insurance</th>
                                        <th>Remarks</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($employeeDependants as $employeeDependant)
                                        <tr>
                                            <td>{{ $employeeDependant->relation->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependant->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependant->date_of_birth ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependant->country->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependant->passport_number ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependant->passport_expiry ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependant->visa_number ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependant->visa_expiry ?? 'N/A' }}</td>
                                            <td>{{ ($employeeDependant->travel_allounce ?? 'N/A' == 1)? 'Active' : 'In-Active' }}</td>
                                            <td>{{ ($employeeDependant->insurance ?? 'N/A' == 1)? 'Active' : 'In-Active' }}</td>
                                            <td>{{ $employeeDependant->remarks ?? 'N/A' }}</td>
                                            <td><button class="btn btn-default btn-sm" data-toggle="modal" data-value="{{ $employeeDependant->employee_id ?? 'N/A' }}" data-target="#employee-dependants"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="13" align="center">No data found</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Employee Dependants End -->
                    <!-- Employee Emergency Contact Details Start -->
                    <div class="tab-pane show" id="contacts">
                        <h6 class='text-warning'><b>Emergency Contact Numbers </b><small>( Minimum 2 contacts required)</small>
                        <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#emergency-contacts"><i class="fa fa-plus"></i> Add</button>
                        </h6>
                        <hr />
                        <div class="responsive">
                            <table class="table m-b-0">
                                <thead>
                                    <tr style="font-weight:normal;">
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Relation</th>
                                        <th>Mobile Number</th>
                                        <th>Home Number</th>
                                        <th>Work Number <small>(If Applicable)</small></th>
                                        <th>Home Address</th>
                                        <th>Description</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($contactPersons as $contactPerson)
                                        <tr>
                                            <td>{{ $contactPerson->full_name ?? 'N/A' }}</td>
                                            <td>{{ $contactPerson->email ?? 'N/A' }}</td>
                                            <td>{{ $contactPerson->relationName->name ?? 'N/A' }}</td>
                                            <td>{{ $contactPerson->mobile_number ?? 'N/A' }}</td>
                                            <td>{{ $contactPerson->home_phone_number ?? 'N/A' }}</td>
                                            <td>{{ $contactPerson->work_number ?? 'N/A' }}</td>
                                            <td>{{ $contactPerson->address ?? 'N/A' }}</td>
                                            <td>{{ $contactPerson->description ?? 'N/A' }}</td>
                                            <td><button class="btn btn-default btn-sm" data-toggle="modal" data-value="{{ $contactPerson->id ?? 'N/A' }}" data-target="#emergency-contacts"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="8" align="center">No data found</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Employee Emergency Contact Details End -->
                    <!-- Employee Education Details -->
                    <div class="tab-pane show" id="empeducation">
                        <h6 class='text-warning'><b>Employee Education Details</b> <small>(Upload Documents where applicable)</small>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-education"><i class="fa fa-plus"></i> Add</button>
                        </h6>
                        <hr />
                        <div class="table-responsive ">
                            <table class="table m-b-0">
                                <thead>
                                    <tr style="font-weight:normal;">
                                        <th>Qualification</th>
                                        <th>Institute</th>
                                        <th>Country</th>
                                        <th>Marks/Grade</th>
                                        <th>Year</th>
                                        <th>Attested</th>
                                        <th>Hard Copy</th>
                                        <th>Document</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($educations as $education)
                                    <tr>
                                        <td>{{ $education->qualification->name ?? 'N/A' }} </td>
                                        <td>{{ $education->institute->name  ?? 'N/A'}}</td>
                                        <td>{{ $education->country->name ?? 'N/A' }}</td>
                                        <td>{{ $education->marks ?? 'N/A' }}</td>
                                        <td>{{ $education->completed_date ?? 'N/A' }}</td>
                                        <td>{{ ($education->attested  == 1)? 'Attested' :  'Not Attested'}}</td>
                                        <td>{{ ($education->hardcopy  == 1)? 'Submitted' :  'Not Submitted'}}</td>
                                        <td>
                                            <a target="_blank" class="btn btn-default btn-sm" href="{{asset('media_documents/'.$employee->employee_id."/".$education->degree_image)}}"}}><i class="fa fa-download" aria-hidden="true"></i></a>
                                        </td>
                                        <td>
                                            <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#employee-education"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td colspan="8" align="center">No data found</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Employee Education Details -->
                </div>
                <!--tab-content-->
            </div>
        </div>

        <div class="col-lg-12 col-md-12">
            <div class="card single_post">
                <ul class="nav nav-tabs ">
                    <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#Home-withicon">Official Details</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Profile-withicon"> Rules</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#depbenefits"> Dependant Benefits</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#payelements"> Pay Elements</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#bankinfo"> Bank Info</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#empinsurance"> Insurance</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#empdocuments"> Documents</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#assetsgiven"> Assets</a></li>
                </ul>
                <div class="bg-dark text-light">
                    <div class="tab-content ">
                        <div class="tab-pane show active" id="Home-withicon">
                            <h6 class='text-warning'><b>Official Details </b>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#official-details"><i class="fa fa-pencil-square-o"></i> Edit</button>
                            </h6>
                            <hr />
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-2 pull-left">
                                    <h6>Position Category</h6>
                                    <p>{{ $employee->positionCategory->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Designation</h6>
                                    <p>{{ $employee->designation->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Department</h6>
                                    <p>{{ $employee->department->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Sub Department</h6>
                                    <p>{{ $employee->subDepartment->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Country</h6>
                                    <p>{{ $employee->country->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>State/City</h6>
                                    <p>{{ $employee->city->name ?? 'N/A' }}</p>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-2 pull-left">
                                    <h6>Date of Joining</h6>
                                    <p>{{ $employee->date_of_joining ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Probation Period</h6>
                                    <p>{{ $employee->probation_period ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Probation End Date</h6>
                                    <p>{{ $employee->probation_end_date ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Cost Center</h6>
                                    <p>{{ $employee->costCenter->name ?? 'N/A'  }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Project/Client</h6>
                                    <p>{{ $employee->project->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Is Supervisor?</h6>
                                    <p>{{ ($employee->supervisor == 1)? 'Yes' : 'No' }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 pull-left">
                                    <h6>Exit Date</h6>
                                    <p>{{ $employee->date_of_exit ?? 'N/A' }}</p>
                                </div>
                            </div>
                            {{-- <div class="table-responsive">
                                <table class="table m-b-0">
                                    <thead>
                                        <tr style="font-weight:normal;">
                                            <th>Position Category</th>
                                            <th>Designation</th>
                                            <th>Department</th>
                                            <th>Sub Department</th>
                                            <th>Country</th>
                                            <th>State/City</th>
                                            <th>Date of Joining</th>
                                            <th>Probation Period</th>
                                            <th>Probation End Date</th>
                                            <th>Cost Center</th>
                                            <th>Project/Client</th>
                                            <th>Is Supervisor?</th>
                                            <th>Exit Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $employee->employee_category->name ?? 'N/A' }}</td>
                                            <td>{{ $employee->designation->name ?? 'N/A' }}</td>
                                            <td>{{ $employee->department->name ?? 'N/A'}}</td>
                                            <td>{{ $employee->sub_department->name ?? 'N/A' }}</td>
                                            <td>{{ $employee->country->name ?? 'N/A' }}</td>
                                            <td>{{ $employee->city->name ?? 'N/A' }}</td>
                                            <td>{{ $employee->date_of_joining }}</td>
                                            <td>{{ $employee->probation_period }}</td>
                                            <td>{{ $employee->probation_end_data }}</td>
                                            <td>{{ $employee->cost_center->name ?? 'N/A' }}</td>
                                            <td>{{ $employee->project->name ?? 'N/A' }}</td>
                                            <td>{{ $employee->supervisor }}</td>
                                            <td>{{ $employee->date_of_exit }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> --}}
                        </div>
                        <!-- Official Details End -->
                        <div class="tab-pane" id="Profile-withicon">
                            <h6 class="text-warning"><b>Rules</b>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-rules"><i class="fa fa-pencil-square-o"></i> Edit</button>
                            </h6>
                            <hr />
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-2 pull-left">
                                    <h6>Contract Type</h6>
                                    <p>{{ $employee->contractTypes->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Notice Period</h6>
                                    <p>{{ $employee->designation->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Shift</h6>
                                    <p>{{ $employee->department->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Leave Rule</h6>
                                    <p>{{ $employee->subDepartment->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Gratuity Rule</h6>
                                    <p>{{ $employee->country->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Ticket Entitlement</h6>
                                    <p>{{ $employee->city->name ?? 'N/A' }}</p>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-2 pull-left">
                                    <h6>Route</h6>
                                    <p>{{ $employee->positionCategory->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Max. Fare Rate <small>(If Applicable)</small></h6>
                                    <p>{{ $employee->designation->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Gratuity</h6>
                                    <p>{{ $employee->subDepartment->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Air Fare</h6>
                                    <p>{{ $employee->country->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Leave Salary</h6>
                                    <p>{{ $employee->city->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Medical Insurance</h6>
                                    <p>{{ $employee->department->name ?? 'N/A' }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 pull-left">
                                    <h6>Visa Processing Cost</h6>
                                    <p>{{ $employee->department->name ?? 'N/A' }}</p>
                                </div>
                            </div>
                            {{-- <div class="table-responsive">
                                <table class="table m-b-0">
                                    <thead>
                                        <tr style="font-weight:normal;">
                                            <th>Contract Type</th>
                                            <th>Notice Period</th>
                                            <th>shift</th>
                                            <th>Leave Rule</th>
                                            <th>Gratuity Rule</th>
                                            <th>Ticket Entitlement</th>
                                            <th>Route</th>
                                            <th>Max. Fare Rate <small>(If Applicable)</small></th>
                                            <th>Gratuity</th>
                                            <th>Air Fare</th>
                                            <th>Leave Salary</th>
                                            <th>Medical Insurance</th>
                                            <th>Visa Processing Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> --}}
                        </div>
                        <!-- Employee Rules End -->
                        <div class="tab-pane" id="depbenefits">
                            <h6 class="text-warning"><b>Dependents Benefits</b>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#dependant-benefits"><i class="fa fa-plus"></i> Add</button>
                            </h6>
                            <hr />    
                            <div class="table-responsive">
                                <table class="table m-b-0">
                                    <thead>
                                        <tr style="font-weight:normal;">
                                            <th>Ticket Entitlement</th>
                                            <th>Name</th>
                                            <th>Route (From - To)</th>
                                            <th>Date (From - To)</th>
                                            <th>Insurance Company</th>
                                            <th>Category</th>
                                            <th>Policy No.</th>
                                            <th>Fare Amount</th>
                                            <th>Ins. Amount</th>
                                            <th>Premium</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($employeeDependantsBenefits as $employeeDependantsBenefit)
                                        <tr>
                                            <td>{{ $employeeDependantsBenefit->ticketEntitlement->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->employeeDependant->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->routeFrom->name ?? 'N/A' }} <i class="fa fa-exchange"></i> {{ $employeeDependantsBenefit->routeTo->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->start ?? 'N/A' }} - {{ $employeeDependantsBenefit->end ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->insuranceCompany->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->insuranceCategory->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->policy_number ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->currency->name  . ' ' . $employeeDependantsBenefit->fare_amount ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->currency->name  . ' ' . $employeeDependantsBenefit->insurance_amount ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->currency->name  . ' ' . $employeeDependantsBenefit->premium ?? 'N/A' }}</td>
                                            <td>{{ $employeeDependantsBenefit->description ?? 'N/A' }}</td>
                                            <td>{{ ($employeeDependantsBenefit->status ?? 'N/A' == 1)? 'Active' : 'In-Active' }}</td>
                                            <td><button class="btn btn-default btn-sm" data-toggle="modal" data-value="1" data-target="#dependant-benefits"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="13"></td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Dependant Benefits End -->
                        <div class="tab-pane" id="payelements">
                            <h6 class="text-warning"><b>Pay Elements</b>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-pay-elements"><i class="fa fa-pencil-square-o"></i> Edit</button>
                            </h6>
                            <hr />
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-2 pull-left">
                                    <h6>Salary Rule</h6>
                                    <p>{{ $employee->positionCategory->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Payment Type</h6>
                                    <p>{{ $employee->designation->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Basic Salary</h6>
                                    <p>{{ $employee->department->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>House Allounce</h6>
                                    <p>{{ $employee->subDepartment->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Insentives</h6>
                                    <p>{{ $employee->country->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Transport Allounce</h6>
                                    <p>{{ $employee->city->name ?? 'N/A' }}</p>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-2 pull-left">
                                    <h6>Mobile Allounce</h6>
                                    <p>{{ $employee->positionCategory->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Others</h6>
                                    <p>{{ $employee->designation->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Last Salary Revision</h6>
                                    <p>{{ $employee->department->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Company Account</h6>
                                    <p>{{ $employee->subDepartment->name ?? 'N/A' }}</p>
                                </div>
                            </div>
                            {{-- <div class="table-responsive">
                                <table class="table m-b-0">
                                    <thead>
                                        <tr style="font-weight:normal;">
                                            <th>Salary Rule</th>
                                            <th>Salary Currency</th>
                                            <th>Payment Type</th>
                                            <th>Basic Salary</th>
                                            <th>House Allounce</th>
                                            <th>Insentives</th>
                                            <th>Transport Allounce</th>
                                            <th>Mobile Allounce</th>
                                            <th>Others</th>
                                            <th>Last Salary Revision</th>
                                            <th>Company Account</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> --}}
                        </div>
                        <!-- Employee Pay Elements End -->
                        <div class="tab-pane" id="bankinfo">
                            <h6 class="text-warning"><b>Employee Bank Information</b>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-bank-account"><i class="fa fa-plus"></i> Add</button>
                            </h6>
                            <hr />
                            <div class="table-responsive">
                                <table class="table m-b-0">
                                    <thead>
                                        <tr style="font-weight:normal;">
                                            <th>Bank Name</th>
                                            <th>Account Number</th>
                                            <th>IBAN Number (23 Digits)</th>
                                            <th>Account Holdar Name</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($bankDetails as $bankDetail)
                                        <tr>
                                            <td>{{ $bankDetail->bank->name ?? 'N/A' }}</td>
                                            <td>{{ $bankDetail->account_no ?? 'N/A' }}</td>
                                            <td>{{ $bankDetail->iban_no ?? 'N/A' }}</td>
                                            <td>{{ $bankDetail->account_holdar_name ?? 'N/A' }}</td>
                                            <td>{{ $bankDetail->description ?? 'N/A' }}</td>
                                            <td>{{ ($bankDetail->status ?? 'N/A' == 1)? 'Active' : 'In-Active' }}</td>
                                            <td><button class="btn btn-default btn-sm" data-toggle="modal" data-value="1" data-target="#employee-bank-account"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
                                        </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7" align="center">No data found</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Employee Bank Information End -->
                        <div class="tab-pane" id="empinsurance">
                            <h6 class="test-warning"><b>Insurance</b>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-insurance"><i class="fa fa-pencil-square-o"></i> Edit</button>
                            </h6>
                            <hr />
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-2 pull-left">
                                    <h6>Insurance Provided</h6>
                                    <p>{{ $employee->positionCategory->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Policy/Company Name</h6>
                                    <p>{{ $employee->designation->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Policy Number</h6>
                                    <p>{{ $employee->department->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Category</h6>
                                    <p>{{ $employee->subDepartment->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Policy Contract Dates</h6>
                                    <p>{{ $employee->country->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>mployee Ensured Dates</h6>
                                    <p>{{ $employee->city->name ?? 'N/A' }}</p>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 20px">
                                <div class="col-2 pull-left">
                                    <h6>Annual Premium <small>(full year rate)</small></h6>
                                    <p>{{ $employee->positionCategory->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Actual Rate</h6>
                                    <p>{{ $employee->designation->name ?? 'N/A' }}</p>
                                </div>
                                <div class="col-2 pull-left">
                                    <h6>Monthly Rate</h6>
                                    <p>{{ $employee->department->name ?? 'N/A' }}</p>
                                </div>
                            </div>
                            {{-- <div class="table-responsive">
                                <table class="table m-b-0">
                                    <thead>
                                        <tr style="font-weight:normal;">
                                            <th>Insurance Provided</th>
                                            <th>Policy/Company Name</th>
                                            <th>Policy Number</th>
                                            <th>Category</th>
                                            <th>Policy Contract Dates</th>
                                            <th>Employee Ensured Dates</th>
                                            <th>Annual Premium <small>(full year rate)</small></th>
                                            <th>Actual Rate</th>
                                            <th>Monthly Rate</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div> --}}
                        </div>
                        <!-- Employee Insurance End -->
                        <div class="tab-pane" id="empdocuments">
                            <h6 class="text-warning"><b>Employee Documents</b>
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-documents"><i class="fa fa-plus"></i> Add</button>
                            </h6>
                            <hr />
                            <div class="table-responsive ">
                                <table class="table m-b-0">
                                    <thead>
                                        <tr style="font-weight:normal;">
                                            <th>Document No.</th>
                                            <th>Status</th>
                                            <th>Document Type</th>
                                            <th>Issue Date</th>
                                            <th>Expiry Date</th>
                                            <th>Place of Issue</th>
                                            <th>Issued By</th>
                                            <th>Notify</th>
                                            <th>Document</th>
                                            <th>Description</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($employeeDocuments as $employeeDocument)
                                        <tr>
                                            <td>{{ $employeeDocument->document_number ?? 'N/A' }}</td>
                                            <td>{{ $employeeDocument->documentStatus->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDocument->documentType->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDocument->issue_date ?? 'N/A' }}</td>
                                            <td>{{ $employeeDocument->expiry_date ?? 'N/A' }}</td>
                                            <td>{{ $employeeDocument->placeOfIssue->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeDocument->issuedBy->name ?? 'N/A' }}</td>
                                            <td>In {{ $employeeDocument->notify_period_in_days ?? 'N/A' }} Days</td>
                                            <td>{{ $employeeDocument->document_path ?? 'N/A' }}</td>
                                            <td>{{ $employeeDocument->description ?? 'N/A' }}</td>
                                            <td><button class="btn btn-default btn-sm" data-toggle="modal" data-value="1" data-target="#employee-documents"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="10"></td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Employee Documents End -->
                        <div class="tab-pane" id="assetsgiven">
                            <h6 class="text-warning">Assets given to Employee
                            <button class="btn btn-primary pull-right ml-auto" data-toggle="modal" data-target="#employee-assets"><i class="fa fa-plus"></i> Add</button>
                            </h6>
                            <hr />
                            <div class="table-responsive ">
                                <table class="table m-b-0">
                                    <thead>
                                        <tr style="font-weight:normal;">
                                            <th>Asset Type</th>
                                            <th>Brand</th>
                                            <th>Serial #</th>
                                            <th>Condition</th>
                                            <th>Value</th>
                                            <th>Description</th>
                                            <th>Issue Date</th>
                                            <th>Return Date</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($employeeAssets as $employeeAsset)
                                        <tr>
                                            <td>{{ $employeeAsset->assetType->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeAsset->assetBrand->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeAsset->serial_no ?? 'N/A' }}</td>
                                            <td>{{ $employeeAsset->assetCondition->name ?? 'N/A' }}</td>
                                            <td>{{ $employeeAsset->currency->name . ' ' . $employeeAsset->value ?? 'N/A' }}</td>
                                            <td>{{ $employeeAsset->description ?? 'N/A' }}</td>
                                            <td>{{ $employeeAsset->issue_date ?? 'N/A' }}</td>
                                            <td>{{ $employeeAsset->return_date ?? 'N/A' }}</td>
                                            <td><button class="btn btn-default btn-sm" data-toggle="modal" data-value="1" data-target="#employee-assets"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="9"></td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Employee Assets End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    @include('employees.modals.profile-image')
    @include('employees.modals.employee-main')
    @include('employees.modals.employee-personal-information')
    @include('employees.modals.employee-contact')
    @include('employees.modals.employee-dependant')
    @include('employees.modals.employee-education')
    @include('employees.modals.emergency-contacts')
    @include('employees.modals.employee-official-details')
    @include('employees.modals.employee-pay-elements')
    @include('employees.modals.employee-bank-account')
    @include('employees.modals.dependant-benefits')
    @include('employees.modals.employee-rules')
    @include('employees.modals.employee-insurance')
    @include('employees.modals.employee-documents')
    @include('employees.modals.employee-assets')
@endsection

@section('extra-js')

<script src="{{ asset('assets/js/pages/forms/dropify.js') }}"></script>
<script src="{{ asset('assets/vendor/dropify/js/dropify.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $("#showForm").click(function() {
            $('.profileDetails').toggleClass('hideDiv');
            $('.editFormArea').toggleClass('hideDiv');
            $('#updateForm').toggleClass('hideDiv');
            $(this).toggleClass('hideDiv');
        });

        $('#updateForm').click(function() {
            var data = {
                first_name: $("#first_name").val(),
                middle_name: $("#middle_name").val(),
                last_name: $("#last_name").val(),
                personal_id: $("#personal_id").val(),
                emirates_id: $("#emirates_id").val(),
                employee_type_id: $("#employee_type_id").val(),
                visa_status_id: $("#visa_status").val(),
                employee_category_id: $("#employee_category_id").val(),
                status: $("#employee_status_id").val()
            };
            $.ajax({
                url: "{{ route('employees.update', [$employee]) }}",
                method: 'PUT',
                data: data,
                success: function(data) {
                    location.reload();
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText
                    alert('Error - ' + errorMessage);
                }
            });
        });

        var toggleForms = function() {
            $("#showForm").toggleClass('hideDiv');
            $("#updateForm").toggleClass('hideDiv');
            $(".profileDetails").toggleClass('hideDiv');
            $(".editFormArea").toggleClass('hideDiv');
        };

        intializeDatePicker('#dateOfCompletion', {
            format:"yyyy",
            minViewMode:"decade",
            autoclose:true
        })
    });

</script>
@endsection
