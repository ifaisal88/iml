<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            @php
                $path = Storage::disk('media_images')->exists(Auth::user()->employee_id . '/profile.jpg') ? asset('media_images/' . Auth::user()->employee_id . '/profile.jpg') : asset('media_images/profile.jpg');
            @endphp
            <img src="{{ $path }}" class="rounded-circle user-photo" alt="Profile Picture" />
            <div class="dropdown">
                <span>Welcome, </span> <br />
                <strong>{{ Auth::User()->first_name }}</strong>
            </div>
            <hr>
            <div class="row">
                <div class="col-4">
                    <h6>5+</h6>
                    <small>Experience</small>
                </div>
                <div class="col-4">
                    <h6>400+</h6>
                    <small>Employees</small>
                </div>
                <div class="col-4">
                    <h6>80+</h6>
                    <small>Clients</small>
                </div>
            </div>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#hr_menu">HR</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#setting"><i class="icon-settings"></i></a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content p-l-0 p-r-0">
            <div class="tab-pane animated fadeIn active" id="hr_menu">
                <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                        <li class="active"><a href="{{ route('dashboard') }}"><i class="icon-speedometer"></i><span>HR
                                    Dashboard</span></a></li>
                        <li><a href="app-holidays.html"><i class="icon-list"></i>Holidays</a></li>
                        <li><a href="app-events.html"><i class="icon-calendar"></i>Events</a></li>
                        <li><a href="app-activities.html"><i class="icon-badge"></i>Activities</a></li>
                        <li><a href="app-social.html"><i class="icon-globe"></i>HR Social</a></li>
                        <li>
                            <a href="#Employees" class="has-arrow"><i class="icon-users"></i><span>Employees</span></a>
                            <ul>
                                <li><a href="{{ route('employees.index') }}">All Employees</a></li>
                                <li><a href="emp-leave.html">Leave Requests</a></li>
                                <li><a href="emp-attendance.html">Attendance</a></li>
                                <li><a href="emp-departments.html">Departments</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#Accounts" class="has-arrow"><i
                                    class="icon-briefcase"></i><span>Accounts</span></a>
                            <ul>
                                <li><a href="acc-payments.html">Payments</a></li>
                                <li><a href="acc-expenses.html">Expenses</a></li>
                                <li><a href="acc-invoices.html">Invoices</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#Payroll" class="has-arrow"><i
                                    class="icon-credit-card"></i><span>Payroll</span></a>
                            <ul>
                                <li><a href="payroll-payslip.html">Payslip</a></li>
                                <li><a href="payroll-salary.html">Employee Salary</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#Report" class="has-arrow"><i class="icon-bar-chart"></i><span>Report</span></a>
                            <ul>
                                <li><a href="report-expense.html">Expense Report</a></li>
                                <li><a href="report-invoice.html">Invoice Report</a></li>
                            </ul>
                        </li>
                        <li><a href="app-users.html"><i class="icon-user"></i>Users</a></li>
                        <li>
                            <a href="#Authentication" class="has-arrow"><i
                                    class="icon-lock"></i><span>Authentication</span></a>
                            <ul>
                                <li><a href="page-login.html">Login</a></li>
                                <li><a href="page-register.html">Register</a></li>
                                <li><a href="page-lockscreen.html">Lockscreen</a></li>
                                <li><a href="page-forgot-password.html">Forgot Password</a></li>
                                <li><a href="page-404.html">Page 404</a></li>
                                <li><a href="page-403.html">Page 403</a></li>
                                <li><a href="page-500.html">Page 500</a></li>
                                <li><a href="page-503.html">Page 503</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="tab-pane animated fadeIn" id="setting">
                <div class="tab-pane animated fadeIn" id="sub_menu">
                    <nav class="sidebar-nav">
                        <ul class="main-menu metismenu">
                            <li>
                                <a href="#Maps" class="has-arrow"><i class="icon-briefcase"></i> <span>Assets</span></a>
                                <ul>
                                    <li><a href="">Brands</a></li>
                                    <li><a href="">Condition</a></li>
                                    <li><a href="">Types</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
