<x-header></x-header>
<!-- Page Loader -->
<x-page-loader></x-page-loader>
<!-- Overlay For Sidebars -->
<div id="wrapper">
    @yield('extra-css')
    <!-- Top Navigation -->
    <x-top-nav></x-top-nav>
    <!-- Sidebar Navigation -->
    <x-sidebar></x-sidebar>
    @yield('content')
    <x-footer></x-footer>
    @yield('extra-js')