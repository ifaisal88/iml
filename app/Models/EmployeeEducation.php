<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeEducation extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'qualification_id',
        'institute_id',
        'country_id',
        'marks',
        'completed_date',
        'attested',
        'hardcopy',
        'degree_image',
        'description',
        'created_by',
        'updated_by'
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function qualification()
    {
        return $this->belongsTo(Qualification::class);
    }

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }
}
