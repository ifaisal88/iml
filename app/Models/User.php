<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    public $incrementing = false;

    protected $primaryKey = 'employee_id';

    protected $fillable = [
        'employee_id',
        'personal_id',
        'emirates_id',
        'employee_type_id',
        'visa_status_id',
        'employee_category_id',
        'employee_status_id',
        'line_manager_id',
        'company_id',
        'sponsor_id',
        'pay_group_id',
        'nationality_id',
        'religion_id',
        'department_id',
        'sub_department_id',
        'designation_id',
        'created_by',
        'updated_by',
        'maritial_status',
        'date_of_birth',
        'blood_group',
        'maiden_name',
        'gender',
        'first_name',
        'middle_name',
        'last_name',
        'full_name',
        'email',
        'profile_image',
        'password',
        'personal_email',
        'mobile_number',
        'home_phone_number',
        'work_mobile',
        'current_address',
        'home_country_address',
        'date_of_joining',
        'date_of_exit',
        'status',
        'parent_name',
        'probation_period',
        'probation_end_date',
        'project_id',
        'cost_center_id',
        'supervisor'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeActiveUsers($query)
    {
        $query = $query->where('status', true)->get();
        return $query;
    }

    public function employeeType()
    {
        return $this->belongsTo(EmployeeType::class);
    }

    public function employeeCategory()
    {
        return $this->belongsTo(EmployeeCategory::class);
    }

    public function positionCategory()
    {
        return $this->belongsTo(PositionCategory::class);
    }

    public function employeeStatus()
    {
        return $this->belongsTo(EmployeeStatus::class);
    }

    public function linemanager()
    {
        return $this->belongsTo(User::class, 'line_manager_id', 'employee_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function sponsor()
    {
        return $this->belongsTo(Sponsor::class);
    }

    public function payGroup()
    {
        return $this->belongsTo(PayGroup::class);
    }

    public function nationality()
    {
        return $this->belongsTo(Country::class, 'nationality_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function costCenter()
    {
        return $this->belongsTo(CostCenter::class);
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function subDepartment()
    {
        return $this->belongsTo(SubDepartment::class);
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function visaStatus()
    {
        return $this->belongsTo(VisaStatus::class);
    }

    public function dependants()
    {
        return $this->hasMany(EmployeeDependant::class);
    }

    public function educations()
    {
        return $this->hasMany(EmployeeEducation::class);
    }

    public function officalDetails()
    {
        return $this->hasMany(EmployeeOfficialDetail::class);
    }

    public function payElements()
    {
        return $this->hasMany(EmployeePayElement::class);
    }

    public function provisions()
    {
        return $this->hasMany(EmployeeProvision::class);
    }

    public function dependantBenefits()
    {
        return $this->hasMany(EmployeeDependantBenefit::class);
    }

    public function insurance()
    {
        return $this->hasMany(EmployeeInsurance::class);
    }

    public function documents()
    {
        return $this->hasMany(EmployeeDocument::class);
    }

    public function assets()
    {
        return $this->hasMany(EmployeeAsset::class);
    }

    public function bankDetails()
    {
        return $this->hasMany(EmployeeBankDetail::class);
    }

    public function contactPeoples()
    {
        return $this->hasMany(ContactPeople::class);
    }

    public function employeeBankDetail()
    {
        return $this->hasMany(EmployeeBankDetail::class);
    }

    public function contractTypes()
    {
        return $this->belongsTo(ContractType::class);
    }

    public function employeeDocuments()
    {
        return $this->hasMany(DocumentType::class);
    }

    public function scopeLineManagers($query)
    {
        $managersId = User::whereNotNull('line_manager_id')->select('employee_id')->get()->toArray();
        $query = $query->whereIn('line_manager_id', $managersId);
        return $query;
    }
}