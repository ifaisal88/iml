<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $fillable = [
        'country_id',
        'name',
        'symbol',
        'description',
        'status',
        'created_by',
        'updated_by',
    ];

    public function employeeDependantsBenefits()
    {
        return $this->hasMany(EmployeeDependantBenefit::class);
    }

    public function employeeAssets()
    {
        return $this->hasmany(EmployeeAsset::class);
    }
}
