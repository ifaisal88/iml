<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeInsurance extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'insurance_provider_id',
        'insurance_company_id',
        'policy_name',
        'insurance_category_id',
        'employee_insured_start_date',
        'employee_insured_end_date',
        'annual_premium_per_year_rate',
        'actual_rate',
        'monthly_rate',
        'description',
        'created_by',
        'updated_by'
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }
}
