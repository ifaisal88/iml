<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeAsset extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'asset_type_id',
        'asset_brand_id',
        'serial_no',
        'asset_condition_id',
        'value',
        'currency_id',
        'issue_date',
        'return_date',
        'description',
        'created_by',
        'updated_by',
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function assetBrand()
    {
        return $this->belongsTo(AssetBrand::class);
    }

    public function assetCondition()
    {
        return $this->belongsTo(AssetCondition::class);
    }

    public function assetType()
    {
        return $this->belongsTo(AssetType::class);
    }
}