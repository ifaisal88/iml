<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class country extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'description',
        'status',
        'created_by',
        'updated_by',
    ];

    public function employees()
    {
        return $this->hasMany(User::class, 'nationality_id', 'id');
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function educations()
    {
        return $this->hasMany(EmployeeEducation::class);
    }

    public function employeeDocument()
    {
        return $this->hasMany(EmployeeDocument::class);
    }
}
