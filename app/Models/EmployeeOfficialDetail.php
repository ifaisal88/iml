<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeOfficialDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'position_category_id',
        'designation_id',
        'department_id',
        'sub_department_id',
        'country_id',
        'city_id',
        'date_of_joining',
        'probition_period',
        'probition_end_date',
        'project_id',
        'is_supervisor',
        'exit_date',
        'description',
        'created_by',
        'updated_by'
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }
}
