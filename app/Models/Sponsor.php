<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    use HasFactory;

    protected $fillable = [
        'country_id',
        'name',
        'description',
        'status',
        'created_by',
        'updated_by',
    ];

    public function sponserdEmployees()
    {
        return $this->hasMany(User::class);
    }
}
