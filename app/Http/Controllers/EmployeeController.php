<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\EmployeeType;
use App\Models\VisaStatus;
use App\Models\EmployeeCategory;
use App\Models\Company;
use App\Models\Sponsor;
use App\Models\PayGroup;
use App\Models\Country;
use App\Models\City;
use App\Models\Relation;
use App\Models\Religion;
use App\Models\EmployeeDependant;
use App\Models\Qualification;
use App\Models\Institute;
use App\Models\Bank;
use App\Models\DocumentType;
use App\Models\DocumentStatus;
use App\Models\AssetType;
use App\Models\AssetCondition;
use App\Models\AssetBrand;
use App\Models\Currency;
use App\Models\InsuranceCategory;
use App\Models\InsuranceCompany;
use App\Models\Route;
use App\Models\PositionCategory;
use App\Models\Designation;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\CostCenter;
use App\Models\Project;
use App\Models\TicketEntitlement;
use App\Models\EmployeeStatus;
use App\Models\ContractType;
use App\Models\Shift;
use App\Models\LeavePerAnnum;
use App\Http\Requests\EditUserProfile;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = User::get()->all();
        return view('employees.list', ['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employee = User::create([
            'employee_id' => 'EMP_ID_004',
            'personal_id' => $request->personal_id,
            'emirates_id' => $request->emirates_id,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'full_name' => $request->first_name . ' ' . $request->middle_name . ' ' . $request->last_name,
            'email' => $request->email,
            'password' => Hash::make('password'),
            'home_phone_number' => $request->home_phone_number,
            'profile_image' => 'default_image.jpg',
        ]);
        return redirect()->route('employees.index')->with('status', 'Employee Created Successfuly!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(user $employee)
    {
        // return [$employee->dependants];
        $employee = User::with([
            'employeeType',
            'visaStatus',
            'employeeCategory',
            'contactPeoples.relationName',
            'linemanager',
            'dependants.relation',
            'educations.country',
            'educations.qualification',
            'employeeBankDetail'
        ])->find($employee->employee_id);
        $contactPersons = $employee->contactPeoples;
        $educations = $employee->educations;
        $employeeDependants = $employee->dependants;
        $employeeEducations = $employee->education;
        $employeeDocuments = $employee->documents;
        $employeeDependantsBenefits = $employee->dependantBenefits;
        $employeeAssets = $employee->assets;
        $bankDetails = $employee->bankDetails;
        $employee_types = EmployeeType::select(['id', 'name'])->get();
        $employee_categories = EmployeeCategory::select(['id', 'name'])->get();
        $visa_statuses = VisaStatus::select(['id', 'name'])->get();
        $line_managers = User::select(['employee_id', 'full_name'])->get();
        $companies = Company::select(['id', 'name'])->get();
        $sponsors = Sponsor::select(['id', 'name'])->get();
        $pay_groups = PayGroup::select(['id', 'name'])->get();
        $nationalities = Country::select(['id', 'name'])->get();
        $relations = Relation::select(['id', 'name'])->get();
        $religions = Religion::select(['id', 'name'])->get();
        $document_statuses = DocumentStatus::select(['id', 'name'])->get();
        $document_types = DocumentType::select(['id', 'name'])->get();
        $asset_types = AssetType::select(['id', 'name'])->get();
        $asset_conditions = AssetCondition::select(['id', 'name'])->get();
        $asset_brands = AssetBrand::select(['id', 'name'])->get();
        $banks = Bank::select(['id', 'name'])->get();
        $qualifications = Qualification::select(['id', 'name'])->get();
        $institutes = Institute::select(['id', 'name'])->get();
        $currencies = Currency::select(['id', 'name', 'symbol'])->get();
        $employee_dependent_names = EmployeeDependant::select(['id', 'name'])->where('user_employee_id', $employee->employee_id)->get();
        $insurance_companies = InsuranceCompany::select(['id', 'name'])->get();
        $insurance_categories = InsuranceCategory::select(['id', 'name'])->get();
        $ticket_entitlements = TicketEntitlement::select(['id', 'name'])->get();
        $routes = Route::select(['id', 'name'])->get();
        $positionCategories = PositionCategory::select(['id', 'name'])->get();
        $designations = Designation::select(['id', 'name'])->get();
        $departments = Department::select(['id', 'name'])->get();
        $subDepartments = SubDepartment::select(['id', 'name'])->get();
        $cities = City::select(['id', 'name', 'country_id'])->get();
        $costCenters = CostCenter::select(['id', 'name'])->get();
        $projects = Project::select(['id', 'name'])->get();
        $employeeStatuses = EmployeeStatus::select(['id', 'name'])->get();
        $contractTypes = ContractType::select(['id', 'name'])->get();
        $shifts = Shift::select(['id', 'name'])->get();
        $leaveRules = LeavePerAnnum::select(['id', 'name'])->get();
        $maritialStatus = config('basic.maritial_status');
        $bloodGroup = config('basic.blood_group');
        $employeeGender = config('basic.employee_gender');
        $probationPeriod = config('basic.probation_period');

        return view('employees.profile', [
            'contractTypes'             => $contractTypes,
            'shifts'                    => $shifts,
            'leaveRules'                => $leaveRules,
            'countries'                 => $nationalities,
            'employee'                  => $employee,
            'employee_types'            => $employee_types,
            'visa_statuses'             => $visa_statuses,
            'employee_categories'       => $employee_categories,
            'line_managers'             => $line_managers,
            'companies'                 => $companies,
            'sponsors'                  => $sponsors,
            'pay_groups'                => $pay_groups,
            'nationalities'             => $nationalities,
            'relations'                 => $relations,
            'religions'                 => $religions,
            'employeeDependants'        => $employeeDependants,
            'qualifications'            => $qualifications,
            'institutes'                => $institutes,
            'contactPersons'            => $contactPersons,
            'educations'                => $educations,
            'bankDetails'               => $bankDetails,
            'banks'                     => $banks,
            'document_statuses'         => $document_statuses,
            'document_types'            => $document_types,
            'employeeDocuments'         => $employeeDocuments,
            'asset_types'               => $asset_types,
            'asset_conditions'          => $asset_conditions,
            'asset_brands'              => $asset_brands,
            'currencies'                => $currencies,
            'employeeAssets'            => $employeeAssets,
            'employee_dependent_names'  => $employee_dependent_names,
            'insurance_companies'       => $insurance_companies,
            'insurance_categories'      => $insurance_categories,
            'ticket_entitlements'       => $ticket_entitlements,
            'routes'                    => $routes,
            'employeeDependantsBenefits' => $employeeDependantsBenefits,
            'maritialStatus'            => $maritialStatus,
            'bloodGroup'                => $bloodGroup,
            'employeeGender'            => $employeeGender,
            'probationPeriod'           => $probationPeriod,
            'positionCategories'        => $positionCategories,
            'designations'              => $designations,
            'departments'               => $departments,
            'subDepartments'            => $subDepartments,
            'cities'                    => $cities,
            'costCenters'               => $costCenters,
            'projects'                  => $projects,
            'employeeStatuses'          => $employeeStatuses
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(user $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserProfile $request, user $employee)
    {
        try {
            $full_name = $request->first_name . ' ' . $request->middle_name . ' ' . $request->last_name;
            $request->merge(['full_name' => $full_name]);
            $employee->update($request->all());
            return response()->json(['success' => 'Employee Updated']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $employee)
    {
        try {
            $employee->delete();
            return response()->json(['success' => 'Employee Deleted']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    public function storeContactPerson($id, Request $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $contactPerson = \App\Models\ContactPeople::create($request->all());
        return redirect()->back();
    }

    public function storeDependants($id, Request $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $contactPerson = \App\Models\EmployeeDependant::create($request->all());
        return redirect()->back();
    }

    public function storeEducation($id, Request $request)
    {
        $createRecod = false;
        if (!is_null($request->file('degree'))) {
            $file = $request->file('degree');
            $ext = $file->extension();
            $qualification = Qualification::find($request->qualification_id);
            $name = $qualification->name . '.' . $ext;
            $content = File::get($file->getRealPath());
            $path = $id . "/" . $name;
            if (Storage::disk('media_documents')->put($path, $content)) {
                $request->merge(['user_employee_id' => $id, 'degree_image' => $name]);
                $createRecod = true;
            }
        } else {
            $createRecod = true;
        }
        if ($createRecod) {
            $employeeEducation = \App\Models\EmployeeEducation::create($request->all());
        }

        return redirect()->back();
    }

    public function storeAccountDetails($id, Request $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $employeeEducation = \App\Models\EmployeeBankDetail::create($request->all());
        return redirect()->back();
    }

    public function storeDocuments($id, Request $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $employeeDocument = \App\Models\EmployeeDocument::create($request->all());
        return redirect()->back();
    }

    public function storeAssets($id, Request $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $employeeAsset = \App\Models\EmployeeAsset::create($request->all());
        return redirect()->back();
    }

    public function storeDependantBenefits($id, Request $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $employeeDependantBenefits = \App\Models\EmployeeDependantBenefit::create($request->all());
        return redirect()->back();
    }

    public function updateEmployeeMainInfo(Request $request, $id)
    {
        $user = User::find($id);
        $update = $user->update($request->all());
        if ($update) {
            return redirect()->back();
        }
    }

    public function updateEmployeePersonalDetails(Request $request, $id)
    {
        $user = User::find($id);
        $update = $user->update($request->all());
        if ($update) {
            return redirect()->back();
        }
    }

    public function updateEmployeeContactDetails(Request $request, $id)
    {
        $user = User::find($id);
        $update = $user->update($request->all());
        if ($update) {
            return redirect()->back();
        }
    }

    public function updateEmployeeOfficialDetails(Request $request, $id)
    {
        $user = User::find($id);
        $update = $user->update($request->all());
        if ($update) {
            return redirect()->back();
        }
    }

    public function updateEmployeeRules(Request $request, $id)
    {
        dd($request->all());
        $user = User::find($id);
        $update = $user->update($request->all());
        if ($update) {
            return redirect()->back();
        }
    }

    public function updateProfileImage(Request $request, $id)
    {
        $image = $request->file('profile_image');
        $name = 'profile.jpg';
        $path = $id . '/' . $name;
        if (Storage::disk('media_images')->exists($path)) {
            Storage::disk('media_images')->delete($path);
        }
        $img = \Image::make($image);
        $img->encode('png');

        $width = $img->width();
        $height = $img->height();
        $mask = \Image::canvas($width, $height);

        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, false);

        $img->stream();

        Storage::disk('media_images')->put($path, $img);
        return redirect()->back();
    }
}
