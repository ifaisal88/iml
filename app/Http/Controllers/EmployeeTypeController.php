<?php

namespace App\Http\Controllers;

use App\Models\employeeType;
use Illuminate\Http\Request;

class EmployeeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function show(employeeType $employeeType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeType $employeeType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employeeType $employeeType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeType  $employeeType
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeType $employeeType)
    {
        //
    }
}
