<?php

namespace App\Http\Controllers;

use App\Models\positionCategory;
use Illuminate\Http\Request;

class PositionCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\positionCategory  $positionCategory
     * @return \Illuminate\Http\Response
     */
    public function show(positionCategory $positionCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\positionCategory  $positionCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(positionCategory $positionCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\positionCategory  $positionCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, positionCategory $positionCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\positionCategory  $positionCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(positionCategory $positionCategory)
    {
        //
    }
}
