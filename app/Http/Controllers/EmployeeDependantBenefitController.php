<?php

namespace App\Http\Controllers;

use App\Models\employeeDependantBenefit;
use Illuminate\Http\Request;

class EmployeeDependantBenefitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeDependantBenefit  $employeeDependantBenefit
     * @return \Illuminate\Http\Response
     */
    public function show(employeeDependantBenefit $employeeDependantBenefit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeDependantBenefit  $employeeDependantBenefit
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeDependantBenefit $employeeDependantBenefit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeDependantBenefit  $employeeDependantBenefit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employeeDependantBenefit $employeeDependantBenefit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeDependantBenefit  $employeeDependantBenefit
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeDependantBenefit $employeeDependantBenefit)
    {
        //
    }
}
