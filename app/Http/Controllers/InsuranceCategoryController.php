<?php

namespace App\Http\Controllers;

use App\Models\insuranceCategory;
use Illuminate\Http\Request;

class InsuranceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\insuranceCategory  $insuranceCategory
     * @return \Illuminate\Http\Response
     */
    public function show(insuranceCategory $insuranceCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\insuranceCategory  $insuranceCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(insuranceCategory $insuranceCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\insuranceCategory  $insuranceCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, insuranceCategory $insuranceCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\insuranceCategory  $insuranceCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(insuranceCategory $insuranceCategory)
    {
        //
    }
}
