<?php

namespace App\Http\Controllers;

use App\Models\leavePerAnnum;
use Illuminate\Http\Request;

class LeavePerAnnumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\leavePerAnnum  $leavePerAnnum
     * @return \Illuminate\Http\Response
     */
    public function show(leavePerAnnum $leavePerAnnum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\leavePerAnnum  $leavePerAnnum
     * @return \Illuminate\Http\Response
     */
    public function edit(leavePerAnnum $leavePerAnnum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\leavePerAnnum  $leavePerAnnum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, leavePerAnnum $leavePerAnnum)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\leavePerAnnum  $leavePerAnnum
     * @return \Illuminate\Http\Response
     */
    public function destroy(leavePerAnnum $leavePerAnnum)
    {
        //
    }
}
