<?php

namespace App\Http\Controllers;

use App\Models\subDepartment;
use Illuminate\Http\Request;

class SubDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\subDepartment  $subDepartment
     * @return \Illuminate\Http\Response
     */
    public function show(subDepartment $subDepartment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\subDepartment  $subDepartment
     * @return \Illuminate\Http\Response
     */
    public function edit(subDepartment $subDepartment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\subDepartment  $subDepartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subDepartment $subDepartment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\subDepartment  $subDepartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(subDepartment $subDepartment)
    {
        //
    }
}
