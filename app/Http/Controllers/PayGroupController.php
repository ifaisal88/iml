<?php

namespace App\Http\Controllers;

use App\Models\PayGroup;
use Illuminate\Http\Request;

class PayGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PayGroup  $payGroup
     * @return \Illuminate\Http\Response
     */
    public function show(PayGroup $payGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PayGroup  $payGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(PayGroup $payGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PayGroup  $payGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayGroup $payGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PayGroup  $payGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayGroup $payGroup)
    {
        //
    }
}
