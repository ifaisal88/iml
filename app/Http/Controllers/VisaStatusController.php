<?php

namespace App\Http\Controllers;

use App\Models\VisaStatus;
use Illuminate\Http\Request;

class VisaStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VisaStatus  $visaStatus
     * @return \Illuminate\Http\Response
     */
    public function show(VisaStatus $visaStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VisaStatus  $visaStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(VisaStatus $visaStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VisaStatus  $visaStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VisaStatus $visaStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VisaStatus  $visaStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(VisaStatus $visaStatus)
    {
        //
    }
}
