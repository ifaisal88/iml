<?php

namespace App\Http\Controllers;

use App\Models\EducationType;
use Illuminate\Http\Request;

class EducationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EducationType  $educationType
     * @return \Illuminate\Http\Response
     */
    public function show(EducationType $educationType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EducationType  $educationType
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationType $educationType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EducationType  $educationType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationType $educationType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EducationType  $educationType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationType $educationType)
    {
        //
    }
}
