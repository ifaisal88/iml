<?php

namespace App\Http\Controllers;

use App\Models\employeeDependant;
use Illuminate\Http\Request;

class EmployeeDependantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeDependant  $employeeDependant
     * @return \Illuminate\Http\Response
     */
    public function show(employeeDependant $employeeDependant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeDependant  $employeeDependant
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeDependant $employeeDependant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeDependant  $employeeDependant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employeeDependant $employeeDependant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeDependant  $employeeDependant
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeDependant $employeeDependant)
    {
        //
    }
}
