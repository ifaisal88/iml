<?php

namespace App\Http\Controllers;

use App\Models\costCenter;
use Illuminate\Http\Request;

class CostCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\costCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function show(costCenter $costCenter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\costCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function edit(costCenter $costCenter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\costCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, costCenter $costCenter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\costCenter  $costCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy(costCenter $costCenter)
    {
        //
    }
}
