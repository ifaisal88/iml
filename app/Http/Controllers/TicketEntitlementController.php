<?php

namespace App\Http\Controllers;

use App\Models\ticketEntitlement;
use Illuminate\Http\Request;

class TicketEntitlementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ticketEntitlement  $ticketEntitlement
     * @return \Illuminate\Http\Response
     */
    public function show(ticketEntitlement $ticketEntitlement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ticketEntitlement  $ticketEntitlement
     * @return \Illuminate\Http\Response
     */
    public function edit(ticketEntitlement $ticketEntitlement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ticketEntitlement  $ticketEntitlement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ticketEntitlement $ticketEntitlement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ticketEntitlement  $ticketEntitlement
     * @return \Illuminate\Http\Response
     */
    public function destroy(ticketEntitlement $ticketEntitlement)
    {
        //
    }
}
