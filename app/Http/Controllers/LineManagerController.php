<?php

namespace App\Http\Controllers;

use App\Models\LineManager;
use Illuminate\Http\Request;

class LineManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LineManager  $lineManager
     * @return \Illuminate\Http\Response
     */
    public function show(LineManager $lineManager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LineManager  $lineManager
     * @return \Illuminate\Http\Response
     */
    public function edit(LineManager $lineManager)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LineManager  $lineManager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LineManager $lineManager)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LineManager  $lineManager
     * @return \Illuminate\Http\Response
     */
    public function destroy(LineManager $lineManager)
    {
        //
    }
}
