<?php

namespace App\Http\Controllers;

use App\Models\contactPerson;
use Illuminate\Http\Request;

class ContactPersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contactperson = ContactPerson::create($request->all());
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\contactPerson  $contactPerson
     * @return \Illuminate\Http\Response
     */
    public function show(contactPerson $contactPerson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\contactPerson  $contactPerson
     * @return \Illuminate\Http\Response
     */
    public function edit(contactPerson $contactPerson)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\contactPerson  $contactPerson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, contactPerson $contactPerson)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\contactPerson  $contactPerson
     * @return \Illuminate\Http\Response
     */
    public function destroy(contactPerson $contactPerson)
    {
        //
    }
}
